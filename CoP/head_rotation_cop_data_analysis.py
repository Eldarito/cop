import os
import pandas as pd
import numpy as np
from ofsca import ofsca

# replace this string with the path to your data folder
data_folder = "HEAD ROTATION DATA"
formats_to_include = ['.txt']

# dict to convert experiment id to exp label
exp_id_to_exp_label_dict = {
    "a1" : "opened_eyes_1",
    "a2" : "closed_eyes_1",
    "a11" : "opened_eyes_2",
    "a22": "closed_eyes_2",
    "vrg1": "head_rotation_opened_eyes",
    "apv1": "opened_eyes_3",
    "apv11": "opened_eyes_4_1min",
    "a222": "closed_eyes_3",
    "vrg2": "head_rotation_closed_eyes",
    "apv2": "closed_eyes_4",
    "apv22": "closed_eyes_5_1min",
    "a111": "opened_eyes_5",
}

class WiiBoardData:
    def __init__(self, file_path=None):
        """
        Class for reading and resampling Wii Board data.
        Wii board platfrom sampling frequency is not fixed, so interpolation is needed to achieve a stable sampling frequency
        :param file_path: [str] path to Wii Board csv file
        """
        self.time_ms = []
        self.cop_x = []
        self.cop_y = []
        self.resampled_f_hz = 0
        self.resampled_time_ms = []
        self.resampled_cop_x = []
        self.resampled_cop_y = []

        self.read_wii_board_data(file_path)

    def __norm_and_resample_time_vect(self, f_hz, original_time_vect):
        time_vect_norm = (original_time_vect - original_time_vect[0])/1000
        T = time_vect_norm[-1]
        F = f_hz
        dt = 1/F
        time_vect_resampled = np.arange(0, T + dt, dt)
        return time_vect_resampled

    def __resample_data(self, data, time_vect):
        import scipy.signal as sc
        resampled_data, _ = sc.resample(data, len(time_vect), time_vect)
        return resampled_data

    def __remove_spikes_from(self, vector):
        diff = np.diff(vector)
        std = np.std(diff)
        median = np.median(diff)
        centered = diff - median
        spikes = np.array(np.where(np.abs(centered) > 15*std)) + 1
        print(spikes)

        vector[spikes] = np.median(vector)


    def read_wii_board_data(self, file_path):
        # open file and read all lines
        file = open(file_path, mode='r', encoding='utf-8-sig')
        lines = file.readlines()
        file.close()

        # create lists to store data
        cop_x = []
        cop_y = []
        t = []

        # skip first line
        for line in lines[1:]:
            line = line.split('\t')
            t.append(np.float(line[0]))
            cop_x.append(np.float(line[1]))
            cop_y.append(np.float(line[2]))

        # save data to class variables
        self.time_ms = np.array(t)
        self.cop_x = np.array(cop_x)
        self.cop_y = np.array(cop_y)

        # resample data if needed
        # f_hz = 100
        # time_vect_resampled = self.__norm_and_resample_time_vect(f_hz, original_time_vect=np.array(self.time_ms))
        # self.resampled_f_hz = f_hz
        # self.resampled_time_ms = time_vect_resampled
        # self.resampled_cop_x = self.__resample_data(np.array(self.cop_x), time_vect_resampled)
        # self.resampled_cop_y = self.__resample_data(np.array(self.cop_y), time_vect_resampled)

def plot_figures(cop_axis, **kwargs):
    import matplotlib.pyplot as plt
    patient_name=kwargs["patient_name"]
    exp_label=kwargs["exp_label"]
    integrate=kwargs["integrate"]
    exp_id=kwargs["exp_id"]
    label=kwargs["label"]
    fig, (ax1, ax2)=plt.subplots(2, figsize=(10, 10))
    fig.tight_layout(pad=5)
    
    ax1.plot(range(len(cop_axis)), cop_axis, label=label)
    ax1.set_ylabel(label), ax1.set_xlabel("(t)"), ax1.legend()
    for order in range(3):
        log_s, log_F=ofsca.get_cDMA(cop_axis, order=order*2, 
        integrate=integrate)
        ax2.plot(log_s, log_F, label=f"order: {order*2}")
        ax2.set_xlabel("log s"), ax2.set_ylabel("log F")
        ax2.legend()
    if integrate:
        plt.savefig(f"images/integrated/{patient_name}_{exp_id}_{exp_label}_{label}_si.png") #save figure
    else:
        plt.savefig(f"images/not integrated/{patient_name}_{exp_id}_{exp_label}_{label}_bi.png") #save figure
    plt.close()

def perform_analysis(input_file):
    # unzip the contents of each element
    file_path, patient_name, exp_id, exp_label = input_file
    print(f"Performing analysis of {patient_name}, {exp_id}, {exp_label}")

    #read data from file
    cop_data = WiiBoardData(file_path=file_path)

    #calculate what you need here below
    cop_x_std = np.std(cop_data.cop_x)
    cop_y_std = np.std(cop_data.cop_y)
    
    cop_x_mean=np.mean(cop_data.cop_x)
    cop_y_mean=np.mean(cop_data.cop_y)

    #Applies CDMA frunction without integration
    plot_figures(cop_data.cop_x, patient_name=patient_name,
    exp_label=exp_label, exp_id=exp_id, label="x", integrate=False)
    plot_figures(cop_data.cop_y, patient_name=patient_name,
    exp_label=exp_label, exp_id=exp_id, label="y", integrate=False)

    #Applies CDMA frunction with integration
    plot_figures(cop_data.cop_x, patient_name=patient_name,
    exp_label=exp_label, exp_id=exp_id, label="x", integrate=True)
    plot_figures(cop_data.cop_y, patient_name=patient_name,
    exp_label=exp_label, exp_id=exp_id, label="y", integrate=True)

    # ...

    df_dict = {'PATIENT_NAME': patient_name,
                'EXP_ID': exp_id,
                'EXP_LABEL': exp_label,
                'STD_X' : cop_x_std,
                'STD_Y' : cop_y_std,
                'MEAN_X': cop_x_mean,
                'MEAN_Y': cop_y_mean,
               }

    patient_df = pd.DataFrame(df_dict, index=[0])

    return patient_df


if __name__ == "__main__":

    # get the list of all files in directory tree at given path
    files = []
    for (dirpath, dirnames, filenames) in os.walk(data_folder):
        for file in filenames:
            if file.endswith(tuple(formats_to_include)):
                files += [os.path.join(dirpath, file)]

    # get patient name, exp_ids and exp_labels from file_path
    patient_name = [f.split("\\")[-2] for f in files]
    exp_id = [f.split("\\")[-1].split("_")[0] for f in files]
    exp_label = [exp_id_to_exp_label_dict.get(i) for i in exp_id]

    # zip elements to form a list of tuples
    data_list = [X for X in zip(files, patient_name, exp_id, exp_label)]
    print(data_list)

    # perform analysis for each file and save to the resulting dataframe
    cop_final_df = pd.DataFrame()
    for data in data_list:
        result_df = perform_analysis(data)
        cop_final_df = cop_final_df.append(result_df, ignore_index=True)

    # uncomment to save dataframe to csv if needed
    cop_final_df.to_csv(f"cop_final_df.csv", index=False)

    print("Done")